#!/usr/bin/python3
from random import randint
from pprint import pprint
'''
1. dado a matriz , calcular a soma das diagonais
matriz = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]
exemplo= 1+5+9+3+5+7
'''
# Gerando uma matris com numeros aleatorios
matriz = [
    [randint(0,10) for x in range(3)],
    [randint(0,10) for x in range(3)],
    [randint(0,10) for x in range(3)]
]
soma = 0

# Calculando a soma das diagonais
for cont,x in enumerate(matriz):
    soma += x[cont]
    soma += x[-(cont+1)]

# Resultado
pprint(matriz)
print(soma)