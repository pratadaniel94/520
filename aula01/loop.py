#!/usr/bin/python3

# letras = [chr(x) for x in range(97, 123)]
letras = []
for x in range(97, 123):
    letras.append(chr(x))

while letras:
    print(letras.pop(0))

print(letras)