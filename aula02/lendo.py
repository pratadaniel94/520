#!/usr/bin/python3

# with open('nomes.txt', 'r') as arquivo:
#     print(arquivo.readline())
#     print(arquivo.readline())
#     arquivo.seek(0)
#     print(arquivo.readline())
nomes = ['yasmin', 'rafael', 'jessica']

with open('nomes.txt', 'a') as arquivo:
    arquivo.writelines([x+'\n' for x in nomes])

with open('nomes.txt', 'a') as arquivo:
    for nome in nomes:
        arquivo.write(nome +'\n')


# arquivo = open('nomes.txt', 'r')

# print(arquivo.read())

# arquivo.close()