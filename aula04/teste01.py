from math import sqrt


def raiz(num):
    if isinstance(num, int):
        return num ** 0.5
    elif isinstance(num, str):
        # return int(num) ** 0.5 if num.isnumeric() else False
        if num.isnumeric():
            return int(num) ** 0.5
        else:
            return False
    # return sqrt(num)


assert raiz(64) == 8
assert raiz(25) == 5
assert raiz(81) == 9
assert raiz('36') == 6
assert raiz('dsdfs') == False
